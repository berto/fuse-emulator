#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow

BUILD_DIR_GTK=$(CURDIR)/build-gtk
BUILD_DIR_SDL=$(CURDIR)/build-sdl

CONFIGURE_OPTIONS := --with-roms-dir=/usr/share/spectrum-roms \
	--with-bash-completion-dir=/usr/share/bash-completion/completions \
	--enable-desktop-integration --with-audio-driver=sdl

CPPFLAGS = $(shell dpkg-buildflags --get CPPFLAGS) \
	-DGLIB_DISABLE_DEPRECATION_WARNINGS \
	-DGDK_DISABLE_DEPRECATION_WARNINGS

ifeq (,$(filter debug,$(DEB_BUILD_OPTIONS)))
	CPPFLAGS += -DNDEBUG -DG_DISABLE_CAST_CHECKS
endif

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -B$(BUILD_DIR_GTK) -- $(CONFIGURE_OPTIONS) --with-x
	dh_auto_configure -B$(BUILD_DIR_SDL) -- $(CONFIGURE_OPTIONS) --with-sdl

override_dh_auto_build:
	dh_auto_build -B$(BUILD_DIR_GTK) -- EXEEXT=-gtk
	desktop-file-edit --set-key=Name --set-value='Fuse (GTK)' \
		--set-key=Exec --set-value='/usr/bin/fuse-gtk %f' \
		$(BUILD_DIR_GTK)/data/fuse.desktop
	cp $(BUILD_DIR_GTK)/data/fuse.desktop $(BUILD_DIR_GTK)/fuse-gtk.desktop

	dh_auto_build -B$(BUILD_DIR_SDL) -- EXEEXT=-sdl
	desktop-file-edit --set-key=Name --set-value='Fuse (SDL)' \
		--set-key=Exec --set-value='/usr/bin/fuse-sdl %f' \
		--remove-category=GTK $(BUILD_DIR_SDL)/data/fuse.desktop
	cp $(BUILD_DIR_SDL)/data/fuse.desktop $(BUILD_DIR_SDL)/fuse-sdl.desktop

override_dh_auto_install:
	dh_auto_install -B$(BUILD_DIR_GTK) -- EXEEXT=-gtk
	dh_auto_install -B$(BUILD_DIR_SDL) -- EXEEXT=-sdl

override_dh_auto_test:
	dh_auto_test -B$(BUILD_DIR_GTK)
	dh_auto_test -B$(BUILD_DIR_SDL)

override_dh_auto_clean:
	dh_auto_clean -B$(BUILD_DIR_GTK)
	dh_auto_clean -B$(BUILD_DIR_SDL)

include /usr/share/dpkg/pkg-info.mk
VERSION := $(DEB_VERSION_UPSTREAM)
UPSTREAM_VERSION := $(shell echo $(VERSION) | sed 's/+dfsg1//')

get-orig-source:
	dh_testdir
	uscan --rename --download-current-version
	mkdir "fuse-emulator-$(VERSION)"
	tar -C "fuse-emulator-$(VERSION)" \
		--strip-components=1 --exclude=roms \
		-xaf "../fuse-emulator_$(UPSTREAM_VERSION).orig.tar.gz"
	fakeroot tar -caf "../fuse-emulator_$(VERSION).orig.tar.xz" \
		"fuse-emulator-$(VERSION)"
	rm -rf "fuse-emulator-$(VERSION)"
